"use strict"

var cookieParser = require('cookie-parser');
var expressPDF = require('express-pdf');
var mustacheExpress = require('mustache-express');
var mustache = require('mustache');
var csrf = require('csurf');
var bodyParser = require('body-parser');
var config = require('config-yml');
var findInCSV = require('find-in-csv');
var fs = require('fs');
var qr = require('qr-image');
var isEmail = require('isemail');
var path = require('path');

const port = 4000;
const express = require('express');
const app = express();

app.set('port',port);

var csrfProtection = csrf({ cookie: true });
var parseForm = bodyParser.urlencoded({ extended: false });

app.engine('html', mustacheExpress());
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

app.use(expressPDF);
app.use(cookieParser());
app.use('/static', express.static(__dirname + '/assets'));

app.get('/', csrfProtection, (request, response) => {
    var err = null;
    var error = request.query.error;
    var args = config.site;
    args.event = config.event;
    args.formAction = config.routes.certificate;
    args.csrfToken = request.csrfToken();

    if(typeof error !== 'undefined'){
        err = error.replace(/<(?:.|\n|)*?>|\./gm, '');
        if(config.errors[err]){
            args.error = config.errors[err];
        }
    }

    if(null === err){
        args.error = '';
    }

    response.render('index',args);
});

app.get('/detail/:email', csrfProtection, (request, response) => {
    var err = null;
    var error = request.query.error;
    var args = config.site;
    args.event = config.event;
    args.formAction = config.routes.certificate;
    args.csrfToken = request.csrfToken();

    if(typeof error !== 'undefined'){
        err = error.replace(/<(?:.|\n|)*?>|\./gm, '');
        if(config.errors[err]){
            args.error = config.errors[err];
        }
    }

    var csv = new findInCSV(path.resolve(__dirname, './' + config.csv));
    var email = request.params.email;
    var args = {
        baseUrl: request.protocol + '://' + request.get('host'),
        certificate: config.certificate,
        event: config.event
    };

    if(null === err){
        args.error = '';
    }

    csv.get({'email': email}, (result) => {
        if(!result){
            return response.redirect('/?error=emailNoExists');
        } else{
            args.attendee = result;
            return response.render('info',args);
        }
    });
});

app.post('/' + config.routes.certificate, parseForm, csrfProtection, (request, response) => {
    var templatePath = path.resolve(__dirname, './views/pdf.html');
    var csv = new findInCSV(path.resolve(__dirname, './' + config.csv));
    var email = request.body.email;
    var args = {
        baseUrl: request.protocol + '://' + request.get('host'),
        certificate: config.certificate,
        event: config.event
    };

    try{
        if('' === email){
            throw 'missingEmail';
        }
        if(!isEmail.validate(email)){
            throw 'invalidEmail';
        }
    } catch(e){
        return response.redirect('/?error=' + e);
    }

    csv.get({'email': email}, (result) => {
        if(!result){
            return response.redirect('/?error=emailNoExists');
        }

        args.attendee = result;

        var name = result["id"];

        var qr_png = qr.imageSync(request.protocol + '://' + request.get('host') + "/detail/" + email,{ type: 'png', margin: 1});
		let qr_code_file_name = name + '.png';
		fs.writeFileSync('./assets/qr/' + qr_code_file_name, qr_png, (err) => {
			if(err){
                console.log(err);
				return response.redirect('/?error=notAttended');
			}
		});

        /* if('Yes' !== args.attendee.attendance){
            return response.redirect('/?error=notAttended');
        } */

        fs.readFile(templatePath, (err,data) => {
            if(err){
                console.log(err);
                return response.redirect('/?error=csvError');
            }

            args.attendee.data = args.certificate.textLine2
                .replace('%event_name%', '<strong>' + args.event.name + '</strong>')
                .replace('%event_date%', args.event.date)
                .replace('%event_duration%', '<strong>' + args.event.duration + '</strong>');

            args.qr = "static/qr/" + qr_code_file_name;
            
            return response.pdfFromHTML({
                filename: config.routes.certificate + '.pdf',
                htmlContent: mustache.render(data.toString(), args),
                options: {
                    'type': 'pdf',
                    'format': 'A4',
                    'orientation': 'landscape'
                }
            });
        });
    });
});

app.use(function (req, res) {
    var args     = config.site;
    args.event   = config.event;
    args.message = config.errors.error404;
  
    res.status(400);
    res.render('error', args);
});
  
app.use(function (err, req, res, next) {
    if (err.code !== 'EBADCSRFTOKEN') {
        return next(err);
    }
  
    var args     = config.site;
    args.event   = config.event;
    args.message = config.errors.csrfError;
  
    res.status(403);
    res.render('error', args);
  });
  
app.use(function (err, req, res) {
    var args     = config.site;
    args.event   = config.event;
    args.message = config.errors.error500;
  
    res.status(500);
    res.render('error', args);
});

app.listen(app.get("port"), () => {
    console.log('Server listening on port ', app.get('port'));
});